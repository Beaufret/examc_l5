/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_mate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/02 14:25:39 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/02 15:08:34 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_check_piece(char c)
{
	return (c == 'P' || c == 'B' || c == 'Q' || c == 'R');
}

int		ft_b(char **av, int y, int x)
{
	int j;
	int i;
	int len;

	len = ft_strlen (av[1]);
	j = y;
	i = x;
	while (--j >= 1 && ++i < len && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (++j <= len && --i >= 0 && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (--j >= 1 && --i >= 0 && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (++j <= len && ++i < len && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	return (0);
}

int		ft_r(char **av, int y, int x)
{
	int j;
	int i;
	int len;

	len = ft_strlen (av[1]);
	j = y;
	i = x;
	while (--j >= 1 && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (++j <= len && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (--i >= 0 && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	j = y;
	i = x;
	while (++i < len && !ft_check_piece(av[j][i]))
		if (av[j][i] == 'K')
			return (1);
	return (0);
}

int		ft_p(char **av, int y, int x)
{
	int j;
	int i;
	int len;

	len = ft_strlen (av[1]);
	j = y;
	i = x;
	if (av[j - 1][i + 1] && av[j - 1][i + 1] == 'K')
		return (1);
	j = y;
	i = x;
	if (av[j - 1][i - 1] && av[j - 1][i - 1] == 'K')
		return (1);
	return (0);
}

int		ft_check_mate(char **av)
{
	int j;
	int i;

	j = 1;
	while (av[j])
	{
		i = 0;
		while (av[j][i])
		{
			printf("J %i, I %i\n", j, i);
			if (av[j][i] == 'P' && ft_p(av, j, i))
				return (1);
			else if (av[j][i] == 'R' && ft_r(av, j, i))
				return (1);
			else if (av[j][i] == 'B' && ft_b(av, j, i))
				return (1);
			else if (av[j][i] == 'Q' && (ft_r(av, j, i) || ft_b(av, j, i)))
				return (1);
			i++;
		}
		j++;
	}
	return (0);
}

int		main(int ac, char **av)
{
	if (ac == 1)
	{
		write(1, "\n", 1);
		return (0);
	}
	else
	{
		if (ft_check_mate(av) == 1)
			write(1, "Success\n", 8);
		else 
			write(1, "Fail\n", 5);
		return (0);
	}
}
