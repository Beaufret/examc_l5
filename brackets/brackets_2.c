/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brackets_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 13:18:00 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/01 15:12:44 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#define BUFF_SIZE (4096)

void	ft_print_tab(char *tab)
{
	int i;

	i = 0;
	printf("Tab : ");
	while (tab[i])
	{
		printf("%c > ", tab[i]);
		i++;
	}
	printf("\n");
}

int		ft_match(char a, char b)
{
	return ((a == '(' && b == ')') || (a == '[' && b == ']') || (a == '{' && b == '}'));
}

int		ft_bra(char *str)
{
	int		i;
	int		top;
	char	tab[BUFF_SIZE];

	i = 0;
	top = -1;
	while (i < BUFF_SIZE)
	{
		tab[i] = '\0';
		i++;
	}
	i = 0;
	while (str[i])
	{
		//printf("I vaut : %i\n", i);
				if (str[i] == '(' || str[i] == '[' || str[i] == '{')
		{
			top++;
			tab[top] = str[i];
			//ft_print_tab(tab);
		}
		else if (str[i] == ')' || str[i] == ']' || str[i] == '}')
		{
			if (ft_match(tab[top], str[i]) == 0)
				return (0);
			else
				top--;
		}
		i++;
	}
	if (top == -1)
		return (1);
	else 
		return (0);
}

int		main(int ac, char **av)
{
	int i;

	i = 1;
	if (ac > 1)
	{
		while (av[i])
		{
			if (ft_bra(av[i]) == 1)
				write(1, "OK\n", 3);
			else 
				write(1, "Error\n", 6);
			i++;
		}
	}
	else 
		write (1, "\n", 1);
	return (0);
}

