/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnicosia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 18:39:51 by lnicosia          #+#    #+#             */
/*   Updated: 2019/08/01 15:31:41 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	print_memory(const void *addr, size_t size);

int		main(void)
{
	int	tab[10] = {0, 23, 150, 255, 12, 16, 42, 103, 512, 116};

	printf("size of int vaut %lu, size of tab[10] vaut %lu\n", sizeof(int), sizeof(tab));
	print_memory(tab, sizeof(tab));
	return (0);
}
