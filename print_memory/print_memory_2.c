/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 15:22:23 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/01 15:34:56 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_char(unsigned char *p, size_t size, size_t i)
{
	size_t j;

	j = 0;
	while (j < 16 && j < size - i)
	{
		if (*(p + i + j) >= ' ' &&  *(p + i + j) <= '~')
			write (1, p + i + j, 1);
		else 
			write (1, ".", 1);
		j++;
	}
}

void	ft_print_hex(unsigned char *p, size_t size, size_t i)
{
	size_t	j;
	char	tab[16] = "0123456789abcdef";

	j = 0;
	while (j < 16 && j < size - i)
	{
		write (1, tab + *(p + i + j) / 16 % 16, 1);
		write (1, tab + *(p + i + j) % 16, 1);
		if ((j + 1) % 2 == 0)
			write (1, " ", 1);
		j++;
	}
	while (j < 16)
	{
		write (1, "  ", 2);
		if (j % 2 == 0)
			write (1, " ", 1);
		j++;
	}
}

void	print_memory(const void *addr, size_t size)
{
	size_t i;

	i = 0;
	while (i < size)
	{
		ft_print_hex((unsigned char *)addr, size, i);
		ft_print_char((unsigned char *)addr, size, i);
		write (1, "\n", 1);
		i = i + 16;
	}
}
