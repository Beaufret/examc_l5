/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   biggest_pal_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 11:20:18 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/07/17 16:00:20 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	ft_check(char *str, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (str[i] != str[size - i - 1])
			return (0);
		i++;
	}
	return (1);
}

int	main(int ac, char **av)
{
	int i;
	int len;
	int size;
	int start;

	if (ac == 2 && av[1][0] != '\0')
	{
		i = 0;
		while (av[1][i])
			i++;
		len = i;
		size = len;
		//printf("len vaut %i\n", len);
		while (size > 0)
		{
			start = len - size;
			//printf("start vaut %i\n", start);
			while (start >= 0)
			{
				printf("About to check \"%s\" for size %i\n", av[1] + start, size);
				if (ft_check(av[1] + start, size) == 1)
				{
					write(1, av[1] + start, size);
					write(1, "\n", 1);
					return (1);
				}
				start--;
			}
			size--;
		}
		if (ft_check(av[1], len) == 1)
			write(1, av[1], len);
	}
	write(1, "\n", 1);
	return (0);
}
