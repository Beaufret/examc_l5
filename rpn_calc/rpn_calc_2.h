/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc_2.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 18:19:18 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/07/24 19:21:14 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RPN_CALC
# define RPN_CALC

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct s_list	t_list;

struct s_list
{
	int			value;
	t_list		*next;
};

void	ft_lst_del(t_list **list);
int		ft_is_op(char c);
int		ft_is_digit(char *str);
int		ft_is_space(char c);
void	ft_lst_add(t_list **list, int i);
char	*ft_forward(char *str);
int		ft_operate(t_list **list, char c);

#endif
