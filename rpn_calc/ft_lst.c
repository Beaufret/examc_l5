/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_del.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 18:28:42 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/07/24 19:03:09 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rpn_calc_2.h"

void	ft_lst_add(t_list **list, int i)
{
	t_list *link;

	if((link = (t_list *)malloc(sizeof(t_list))))
	{
		link->next = *list;
		link->value = i;
		*list = link;
	}
}

void	ft_lst_del(t_list **list)
{
	t_list	*tmp;
	
	tmp = *list;
	*list = (*list)->next;
	free(tmp);
}
