/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 18:12:32 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/01 13:12:27 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rpn_calc_2.h"

char	*ft_forward(char *str)
{
	while (*str && *str != ' ')
		++str;
	return (str);
}

void	ft_print_lst(t_list *list)
{
	while (list)
	{
		printf("%i > ", list->value);
		list = list->next;
	}
	printf("EOL\n");
}

int		main(int ac, char **av)
{
	char	*str;
	t_list	*list;

	if (ac == 2)
	{
		str = av[1];
		list = NULL;

		while (str)
		{
			printf("STR vaut : %s\n", str);
			ft_print_lst(list);
			if (ft_is_digit(str))
			{
				ft_lst_add(&list, atoi(str));
				str = ft_forward(str);
			}
			else if (ft_is_op(*str))
			{
				if(!ft_operate(&list, *str))
					break;
				str = ft_forward(str);
			}
			else if (ft_is_space(*str))
			{
				if (!*(str + 1))
					break;
				++str;
			}
			else 
				break;
		}
		printf("*str vaut %c\n", *str);
		if (*str)
			write(1, "Error\n", 6);
		else if (list->next)
			write(1, "Error\n", 6);
		else
			printf("%d\n", list->value);
		while (list)
			ft_lst_del(&list);
	}
	else 
		write(1, "Error\n",6);
	return (0);
}
