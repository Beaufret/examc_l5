/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operacte.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 18:50:08 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/07/24 19:08:32 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rpn_calc_2.h"

int		ft_operate(t_list **list, char c)
{
	int a;
	int b;

	if (!*list || !((*list)->next))
		return (0);

	a = ((*list)->next)->value;
	b = (*list)->value;
	if (c == '+')
	{
		((*list)->next)->value = a + b;
		ft_lst_del(list);
		return (1);
	}
	if (c == '-')
	{
		((*list)->next)->value = a - b;
		ft_lst_del(list);
		return (1);
	}
	if (c == '*')
	{
		((*list)->next)->value = a * b;
		ft_lst_del(list);
		return (1);
	}
	if (c == '/')
	{
		if (b == 0)
			return (0);
		((*list)->next)->value = a / b;
		ft_lst_del(list);
		return (1);
	}
	if (c == '%')
	{
		if (b == 0)
			return (0);
		((*list)->next)->value = a % b;
		ft_lst_del(list);
		return (1);
	}
	return (0);
}
