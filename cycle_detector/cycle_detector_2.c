/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cycle_detector_2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 15:57:09 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/01 16:56:24 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list2.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>


long	*ft_cp(long *addresses, int len)
{
	int i;
	long *res;

	i = 0;
	res = (long *)malloc(sizeof(long) * len);
	while (i < len - 1)
	{
		res[i] = addresses[i];
		i++;
	}
	free(addresses);
	return (res);
}

int		ft_match(long tmp, long *addresses, int len)
{
	int i;

	i = 0;
	while (i < len)
	{
		printf("addresses[%i] vaut %li et tmp vaut %li\n", i, addresses[i], tmp);
		if (addresses[i] == tmp)
			return (1);
		i++;
	}
	return (0);
}


int		cycle_detector_2(const t_list *list)
{
	int				len;
	t_list			*tmp;
	long			*addresses;

	len = 0;
	tmp = (t_list *)list;
	addresses = (long *)malloc(sizeof(long));
	printf("Coucou ca commence \n");
	while (tmp)
	{
		printf ("Coucou je boucle len = %i\n", len);
		if (ft_match((long)tmp, addresses, len) == 1)
			return (1);
		len++;
		addresses = ft_cp(addresses, len);
		addresses[len - 1] = (long)tmp;
		tmp = tmp->next;
	}
	return (0);
}
