/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 15:52:00 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/08/01 16:56:19 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "list2.h"

int		cycle_detector_2(const t_list *list);

int		main(void)
{
	t_list *lk1;
	t_list *lk2;
	t_list *lk3;

	lk1 = (t_list*)malloc(sizeof(t_list));
	lk1->data = 1;

	lk2 = (t_list*)malloc(sizeof(t_list));
	lk1->data = 2;

	lk3 = (t_list*)malloc(sizeof(t_list));
	lk1->data = 3;

	lk1->next = lk2;
	lk2->next = lk3;
	lk3->next = NULL;
	//lk3->next = lk1;
	//lk1 = NULL;
	printf("Result is %i \n", cycle_detector_2(lk1));
	return (0);
}
